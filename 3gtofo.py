import igraph
from igraph import *

import openpyxl
from openpyxl import *

import warnings

import time




#################
#
#     CLASES
#
#################
class Sitio():
    def __init__(self, cellid, nombre, tipo):
        self.cellid = cellid
        self.nombre = nombre
        self.tipo = tipo

    def add_to_graph(self, g):
        g.add_vertex( self.cellid )

    def is_FO(self, nodos_fo):
        if self.cellid in nodos_fo:
            return True
        
    def is_3G(self, nodos_3g):
        if self.cellid in nodos_3g:
            return True



class Enlace():
    def __init__(self, ident, cellida, cellidb):
        self.ident = ident
        self.cellida = cellida
        self.cellidb = cellidb

    def add_to_graph(self, g):
        g.add_edge( self.cellida, self.cellidb )


class Ruta():
    def __init__(self, origen, destino, vpath, epath):
        self.origen = origen
        self.destino = destino
        self.vpath = vpath
        self.epath = epath
        
    def hops(self):
        return len(self.epath)




#################
#
#   FUNCIONES
#
#################
def obtener_datos(archivo, hoja1, hoja2, hoja3):
    '''
    La funcion lee las hojas del libro Excel y devuelve una serie de listas.
    INPUT:  *archivo: nombre del archivo Excel
            *hoja1: nombre de la hoja con los Sitios 3G
            *hoja2: nombre de la hoja con los enlaces de MMWW (sitios + links)
            *hoja3: nombre de la hoja con los Sitios FO
    OUTPUT:  *nodos: lista con los CellID de los nodos del grafo a construir
             *nodo_nombre: diccionario con el nombre de los sitios. KEY = nodos
             *links: lista de tuplas de nombres de nodos que forman un enlace.
             *link_id: diccionario ID de enlace. KEY = links
             *link_tipo: diccionario tipo de enlace. KEY = links
             *nodos_fo: lista con los nombres de los nodos DESTINO DE LA RUTA
             *nodos_3g: lista con los nombres de los nodos ORIGEN DE LA RUTA
    '''
    # Abro libro Excel
    file = openpyxl.load_workbook( archivo )

    # NODOS 3G
    sheet = file[ hoja1 ]
    nodos_3g = []
    for i in range(2, sheet.max_row+1):
        nodos_3g.append( sheet.cell(row=i, column=1).value )

    # ENLACES
    sheet = file[ hoja2 ]
    nodos = []
    nodo_nombre = {}
    links = []
    link_id = {}
    link_tipo = {}
    for i in range(2, sheet.max_row+1):
        # Sitio A del enlace: cellida
        nodos.append( sheet.cell(row=i, column=2).value )
        
        # Sitio B del enlace: cellidb
        nodos.append( sheet.cell(row=i, column=6).value )

        # Nombre del sitio
        nodo_nombre[ sheet.cell(row=i, column=2).value ] = sheet.cell(row=i, column=3).value
        nodo_nombre[ sheet.cell(row=i, column=6).value ] = sheet.cell(row=i, column=7).value

        # Enlace: (cellida, cellidb)
        links.append( (sheet.cell(row=i, column=2).value, sheet.cell(row=i, column=6).value) )

        # ID enlace: 543
        link_id[ (sheet.cell(row=i, column=2).value, sheet.cell(row=i, column=6).value) ] = sheet.cell(row=i, column=1).value

        # Tipo del enlace: PDH, SAT, ET
        link_tipo[ (sheet.cell(row=i, column=2).value,sheet.cell(row=i, column=6).value) ] = sheet.cell(row=i, column=10).value

    # NODOS DE FIBRA OPTICA
    sheet = file[ hoja3 ]
    nodos_fo = []
    for i in range(2, sheet.max_row+1):
        nodos_fo.append( sheet.cell(row=i, column=1).value )

    # ELIMINO ELEMENTOS DUPLICADOS
    nodos = sorted(list(set(nodos)))
    links = sorted(list(set(links)))
    nodos_fo = sorted(list(set(nodos_fo)))
    nodos_3g = sorted(list(set(nodos_3g)))
    
    return nodos, nodo_nombre, links, link_id, link_tipo, nodos_fo, nodos_3g


def crear_grafo( nodos, nodo_nombre, links, link_id, link_tipo, nodos_fo, nodos_3g ):
    '''
    La funcion crea el grafo de red a partir de las clases Sitio y Enlace.
    INPUT:  * nodos: lista de sitios que forman un enlace de MW. CellIDA y CellIDB de hoja 'Red'
    
            * links: lista de tuplas de 2 elementos, con los CellID de los sitios que forman un enlace.
            * link_id: diccionario con el identificador de cada enlace. KEY = links
            * link_tipo: diccionario con el tipo de cada enlace. KEY = links
            * nodos_fo: lista con los sitios con FO
            * nodos_3g: lista con los sitios con 3G
    OUTPUT:  * g: grafo de red
             * sitios: lista de objetos 'Sitio'
             * enlaces: lista de objetos 'Enlace'
    '''
    # CREO OBJECTOS 'Sitio' en 'sitios'
    print("Creando variables de entrada... ", flush=True, end='')
    sitios = [ Sitio('MAIN','','') ]
    for n in nodos_3g:                              # Agrego a 'sitios' todos los NODOS-3G que forman parte de un enlace
        if n in nodos and n not in sitios:
            sitios.append( Sitio(n, nodo_nombre[n], '3G') )

    for n in nodos_fo:                              # Agrego a 'sitios' todos los NODOS-FO que forman parte de un enlace
        if n in nodos and n not in sitios:
            sitios.append( Sitio(n, nodo_nombre[n], 'FO') )

    for n in nodos:                                 # Agrego a 'sitios' todos los NODOS DE LOS ENLACES
        if n not in sitios:
            sitios.append( Sitio(n, nodo_nombre[n], 'MW') )


    # CREO OBJECTOS 'Enlace' en 'enlaces'
    enlaces = []
    for link in links:
        enlaces.append( Enlace( link_id[link], link[0], link[1])  )
    print("OK!")

    # CREO GRAFO
    print("Generando grafo... ", flush=True, end='')
    g = Graph()

    for v in sitios:
        v.add_to_graph(g)
        # Si se trata de un sitio de FO, creo un enlace entre ese sitio y el sitio 'MAIN'
        if v.is_FO(nodos_fo) == True:
            enlaces.append( Enlace('', v.cellid, 'MAIN') )

    aux = []
    for e in enlaces:
        e.add_to_graph(g)
        aux.append( e.ident )
    g.es['id'] = aux
    print("OK!")
    return g, sitios, enlaces


def rutear( g, site, target='MAIN'):
    '''
    La funcion encuentra la ruta mas corta en el grafo 'g' desde el nodo 'site' hasta 'target'.
    INPUT:  * g: grafo donde se ruteará
            * site: sitio origen del ruteo
            * target: sitio destino del ruteo
    OUTPUT:  * dest: sitio anterior al sitio destino del ruteo
             * vpath: lista de CellID de sitios por los cuales atraviesa la ruta
             * epath: lista de IDs de enlace por los cuales atraviesa la ruta
    '''
    try:
        vpath = [g.vs[i]['name'] for i in g.get_shortest_paths( site, target, output='vpath')[0]]
        epath = [g.es[i]['id'] for i in g.get_shortest_paths( site, target, output='epath')[0]]
    except:
        print('Error al rutear', site)

    try:
        dest = vpath[-2]
    except:
        dest = '?'          # El sitio 3G no esta conectado a ningun sitio de FO mediante enlace de MW

    vpath = vpath[:len(vpath)-1]
    epath = epath[:len(epath)-1]
    epath.reverse()
    return dest, vpath, epath


def rutear_3G( g, sitios, nodos_3g, nodos_fo):
    '''
    La funcion utiliza la funcion 'rutear' para realizar el ruteo de todos los nodos del grafo que posea 3G
    INPUT:  * g: grafo de red
            * sitios: output de funcion 'crear_grafo'
            * nodos_3g: output de funcion 'obtener_datos'
    OUTPUT: * rutas: lista de objecto 'Ruta'
    '''
    # RUTEO NODOS-3G a NODO-MAIN
    print("Comenzando ruteo...", flush=True, end='')
    rutas = []
    for v in sitios:
        # Solo ruteo aquellos nodos 3G que no sean nodos de FO
        if v.is_3G(nodos_3g) == True and v.is_FO(nodos_fo) != True:
            dest, vpath, epath = rutear( g, v.cellid )
            rutas.append( Ruta( v.cellid, dest, vpath, epath) )
            #print(v.cellid, dest, vpath, epath)
    print("OK!")
    return rutas


def escribir_xlsx(output_file, rutas):
    print("Escribiendo archivo de salida... ", flush=True, end='')
    file = Workbook() 
    sheet = file.create_sheet("Rutas", 0)
    sheet['A1'] = 'Origen (3G)'
    sheet['B1'] = 'Destino (FO)'
    sheet['C1'] = 'Ruta'

    for i in range(len(rutas)):
        sheet.cell(row=2+i, column=1).value = rutas[i].origen
        sheet.cell(row=2+i, column=2).value = rutas[i].destino
        for j in range(len(rutas[i].epath)):
            sheet.cell(row=2+i, column=3+j).value = rutas[i].epath[j]

    file.save(output_file)
    print("OK!")
    return




#################
#
#      MAIN
#
#################
warnings.simplefilter('ignore')

input_file = 'Analisis_Mar18.xlsx'
sheet1 = 'Nodos 3G'
sheet2 = 'Red'
sheet3 = 'Sitios FO'
output_file = input_file.replace('.xlsx','_OUTPUT.xlsx')


print("3G_SITES -TO- OF_SITES ROUTER")
print("=============================")
print("Programa iniciado:", time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"))
print("----")
nodos, nodo_nombre, links, link_id, link_tipo, nodos_fo, nodos_3g = obtener_datos(input_file, sheet1, sheet2, sheet3)

g, sitios, enlaces = crear_grafo( nodos, nodo_nombre, links, link_id, link_tipo, nodos_fo, nodos_3g )

rutas = rutear_3G(g, sitios, nodos_3g, nodos_fo)

escribir_xlsx(output_file, rutas)
print("----")
print("Programa finalizado:", time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"))
