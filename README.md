# **3G-to-FO ROUTER**

## DESCRIPCION
Rutina de ruteo de sitios 3G a su sitio de FO más cercano, a traves de una red de MW. 

# CONSIDERACIONES
* El grafo de red está compuesto por todos los nodos y enlaces de la hoja *Red*.
* No se rutean aquellos sitios de 3G que además sean sitios de FO.
* En caso que en la planilla *output* el *Destino* muestre el caracter *?* significa que el sitio *Origen* no esta conectado mediante enlaces de MW a ningún sitio de FO.